import csv
from flask import Flask
from faker import Faker

app = Flask(__name__)

@app.route('/')
def index():
    return f'Index page'

@app.route('/requirements')
def get_requirements():
    with open ('requirements.txt') as file:
        data = file.read()
    return data

@app.route('/random_users')
def get_random_users():
    data = ''
    user = Faker()
    for n in range(100):
        data += user.name()
        data += user.address()
    return data

@app.route('/avr_data')
def get_avr_data():
    POUNDS_TO_KG_CONST = 0.453592
    INCHES_TO_CM_CONST = 2.54
    weights = []
    heights = []
    with open('hw.csv') as csv_file:
        read_csv = csv.reader(csv_file, delimiter=',')
        for row in read_csv:
            try:
                weight = float(row[2])
                height = float(row[1])
                weights.append(weight)
                heights.append(height)
            except ValueError:
                continue
    def count_avr(list_data):
        return sum(list_data) / len(list_data)
    return f'Average height of students: {count_avr(heights): .2f} inches ({count_avr(heights)*INCHES_TO_CM_CONST: .2f} cm)\
         and average weight of students: {count_avr(weights): .2f} pounds ({count_avr(weights)*POUNDS_TO_KG_CONST: .2f} kg)'
